# Predicting Missing Product Weight using Optimization Techniques

![web_logo_2021_.png](./web_logo_2021_.png)

GAINSystems Chicago is currently adding a new module to CFO (Continuous Fulfillment Optimization) – a Supply Chain Modelling Tool that optimizes the flow of product through a supply chain network via real-time simulations. One of the major functions of the product is to estimate the freight costs to ship cargo from Origin to Destination. Gross Weight of an item/product is an important parameter to estimate freight costs (especially LTL Shipments in Road freight and LCL Shipments in sea freight), but at times for some items, the weight attribute in Item Master is missing and there are gaps in the transactional data resulting in erroneous output during freight calculation, further hosting problems like additional costs, increased logistics spent & confusion between different stakeholders. 
The objective of the project is to predict gross weight of the Item/Product using Optimization Techniques in order to estimate freight costs correctly. Since the current dataset is under preparing from GAINSystem end customer, Krannert Team focus more on researching different algorithms which define outliers.
## Authors

- [@Rupal Bilaiya](rupal.bilaiya@gmail.com)
- Darsh Romeshkumar Shah
- Sibasis Mohapatra
- Misha Chou
- Gautham Eswaranatarajan

## Acknowledgements

 - https://gainsystems.com/
## Tech Stack

**Language:** Machine Learning Algorithms, Python, R

**Tools:** Pycharm, Rstudio


## Project Description

Please refer to following document:
https://gitlab.com/rupalbilaiya/eli-consulting/-/blob/main/ProjectReport_GAINSystems.docx
## Documentation

[GITLAB Project](https://gitlab.com/rupalbilaiya/eli-consulting)

